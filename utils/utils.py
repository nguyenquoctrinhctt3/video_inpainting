import matplotlib.pyplot as plt
import matplotlib.animation as animation
from IPython.display import display, HTML

from torch.autograd import Variable, grad
import torch
import torch.nn as nn
from torchvision import transforms

import os

def get_n_params(model):
    pp=0
    for p in list(model.parameters()):
        nn=1
        for s in list(p.size()):
            nn = nn*s
        pp += nn
    return pp

def animate_sample(sample, fname=None, do_display=True):
    to_pil = transforms.ToPILImage()
    ims = []
    fig = plt.figure()
    for frame in sample:
        im = plt.imshow(to_pil(frame), animated=True)
        ims.append([im])
    ani = animation.ArtistAnimation(fig, ims, interval=50, blit=True,
                                    repeat_delay=50)
    if fname:
        ani.save(fname, writer='imagemagick')
    plt.close()
    if do_display:
        display(HTML(ani.to_html5_video()))
    
def display_sample(sample, cols=8, scale=1.5):
    to_pil = transforms.ToPILImage()
    rows = (sample.size(0) + cols - 1) // cols
    fig=plt.figure(figsize=(cols*scale, rows*scale))
    for i in range(1, cols*rows +1):
        img = to_pil(sample[i - 1])
        fig.add_subplot(rows, cols, i)
        plt.imshow(img)
        plt.axis('off')
    plt.show()
    
def save_sample(tensor, folder):
    if not os.path.exists(folder):
        os.makedirs(folder)
    for ind, image in enumerate(tensor):
        image = transforms.ToPILImage()(image)
        image.save(os.path.join(folder,f"{ind}.png"))
    
def calc_gradient_penalty(netD, real_data, fake_data, batch_size, device, mask=None):
    alpha = torch.rand(batch_size, 1, 1, 1, 1).cuda(device=device)
    interpolates = alpha * real_data + ((1 - alpha) * fake_data)
    interpolates = Variable(interpolates, requires_grad=True)
    disc_interpolates = netD(interpolates)

    gradients = grad(outputs=disc_interpolates, inputs=interpolates,
                     grad_outputs=torch.ones(disc_interpolates.size()).cuda(device=device),
                     create_graph=True, retain_graph=True, only_inputs=True)[0]
    
    # if mask is not None:
    #     gradients = gradients * mask

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean()

    return gradient_penalty

def masked_l1_loss(output, gt, expanded_mask, back_weight=0.3, device=0):
    loss = nn.L1Loss(size_average=False).cuda(device=device)
    mask_sum, neg_mask_sum = expanded_mask.sum(), (1-expanded_mask).sum()
    if mask_sum == 0: mask_sum = 1
    if neg_mask_sum == 0: neg_mask_sum = 1
    hole_loss = loss(output * expanded_mask, gt * expanded_mask) / mask_sum
    back_loss = loss(output * (1-expanded_mask), gt * (1-expanded_mask)) / neg_mask_sum
    return hole_loss + back_weight * back_loss

