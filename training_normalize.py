import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
import torch.optim as optim
from torch.utils.data import DataLoader
import importlib
from datetime import date

from model import building_blocks
from model import discriminator
from model import generator
from utils import logger
from utils import mask_generator
from utils import tv
from utils import video_dataset
from utils import utils


length = 12
in_channels = 3
train_bs = 8
temporal_k = 3
im_size = 64

device = 1

generator_norm = 'bn'
gated=True
n=32
model_3d = generator._3DCN(norm=generator_norm, gated=gated, n=n).cuda(device=device)
print(f"Initialized a generator model with {utils.get_n_params(model_3d)} parameters.")

critic_norm = 'sn'
critic_out_channels = 1
critic_2d = discriminator.PatchDiscriminator(
    norm=critic_norm, output_channels=critic_out_channels).cuda(device=device)
print(f"Initialized a critic model with {utils.get_n_params(critic_2d)} parameters.")

lr_3d = 1e-4
optimizer_3d = optim.Adam(model_3d.parameters(), lr=lr_3d, betas=(0.5, 0.999))
scheduler_3d = optim.lr_scheduler.ReduceLROnPlateau(
    optimizer_3d, 'min', factor=0.4, patience=150, verbose=True)

lr_critic = 1e-3
optimizer_critic = optim.Adam(filter(lambda p: p.requires_grad, critic_2d.parameters()), lr=lr_critic, betas=(0.5, 0.999))
scheduler_critic = optim.lr_scheduler.ReduceLROnPlateau(
    optimizer_critic, 'min', factor=0.5, patience=150, verbose=True)

dataset = video_dataset.VideoDataset("../data/video/set00/",
                                     im_size=im_size, length=length)
data_loader = DataLoader(dataset, batch_size=train_bs, shuffle=True,
                         pin_memory=False, num_workers=3)
# mean_color=torch.Tensor([0.4014, 0.4118, 0.4001]) * 2 - 1
dataset_mean_color = torch.Tensor([0.4229, 0.4341, 0.4205])
dataset_std_color = torch.Tensor([0.2590, 0.2613, 0.2684])
expanded_mean_color = dataset_mean_color.view(1,3,1,1,1).expand(
    train_bs,3,length,im_size,im_size).to(device)
dataset_normalizer = utils.Normalize(dataset_mean_color, dataset_std_color, device)
dataset_unnormalizer = utils.UnNormalize(dataset_mean_color, dataset_std_color, device)
one = torch.FloatTensor([1]).cuda(device=device)
mone = torch.FloatTensor([-1]).cuda(device=device)

min_stroke_width=10
max_stroke_width=20
area_threshold=0.3
mask_gen = mask_generator.MaskGenerator(
    img_size=im_size, max_strokes=5, min_width=min_stroke_width,
    max_width=max_stroke_width, area_threshold=area_threshold)

adv_weight = 0.12
gp_weight = 5
adv_loss = 'wass'
n_dis = 5

vgg_loss = vgg_losses.VGGLosses(device=device)
perception_weight = 0.1
style_weight = 120

tv_loss = tv.TVLoss()
tv_weight = 0.3

today = date.today().strftime('%d%m')
session = "tv{7}_normalize_nonlocal5_{0}_gated_deeperunetG_{5}_lrelu_hinge2dPatchGAN{6}_{4}_lrelu_advw_{1}_gpw{2}_ndis{3}".format(
today,adv_weight,gp_weight,n_dis,critic_norm,generator_norm,critic_out_channels,tv_weight)
print(session)
log = logger.Logger('./logs_video_inpainting', session)
log_step = 0


for epoch in range(0, 10):
    for i, input_batch in enumerate(data_loader):
        batch_size = input_batch.size(0)
        gt = dataset_normalizer(input_batch.cuda(device=device))
        gt_2d = gt.view(batch_size*length,3,im_size,im_size)
        gt = gt.transpose(1,2)
        if batch_size != train_bs:
            continue
        # TODO: different mask for each sample in a batch.
        mask = mask_gen.generate_mask()
        expanded_mask = mask.expand_as(gt).cuda(device=device)
        distorted = gt * (1-expanded_mask) # + expanded_mean_color * expanded_mask
        input_3d = torch.cat((distorted, expanded_mask[:,0,:,:,:].unsqueeze(1)), dim=1)
        out_3d = model_3d(Variable(input_3d))
        
        # TODO: experiment with updating critic on a different mask
        for p in critic_2d.parameters():
            p.requires_grad = True 
        optimizer_critic.zero_grad()
        expanded_mask_2d = expanded_mask[:,0,:,:,:].contiguous().view(batch_size*length,1,im_size,im_size)
        real_data = Variable(torch.cat([gt_2d, expanded_mask_2d], dim=1))
        E_critic_gt = critic_2d(real_data).mean()
        E_critic_gt.backward(mone)

        pred_2d = (out_3d*expanded_mask + gt*(1-expanded_mask)).transpose(1,2)
        pred_2d = pred_2d.contiguous().view(batch_size*length,3,im_size,im_size)
        fake_data = Variable(torch.cat([pred_2d.detach(), expanded_mask_2d], dim=1))
        E_critic_pred = critic_2d(fake_data).mean()
        E_critic_pred.backward(one)

        if adv_loss == 'wass':
            gp = utils.calc_gradient_penalty(
                critic_2d, real_data.data, fake_data.data, batch_size*length,
                device=device)
            gp.backward(one*gp_weight)
            loss_critic = -E_critic_gt + E_critic_pred + gp_weight*gp
        else:
            raise Exception('Unknown critic loss.')
        optimizer_critic.step()
    
        for p in critic_2d.parameters():
            p.requires_grad = False  # to avoid computation

        if i%n_dis == 0:
            optimizer_3d.zero_grad()
            l1_err = utils.masked_l1_loss(out_3d, Variable(gt), Variable(expanded_mask), device=device)
            E_critic_pred_G = critic_2d(torch.cat([pred_2d, expanded_mask_2d], dim=1)).mean()
            tv_err = tv_loss(pred_2d)
            loss_3d = l1_err + adv_weight*(-E_critic_pred_G) + tv_weight*tv_err
            loss_3d.backward()
            optimizer_3d.step()
        
        if i%50 == 0:
            scheduler_3d.step(loss_3d.item())
            scheduler_critic.step(loss_critic.item())
        
        if i%50 == 0:
            log_step += 1
            
            print(f"*** Step {log_step} ***")
            print("""      D_loss: {0:.4f}, E_critic_gt: {1:.4f}, E_critic_pred: {2:.4f}, GP: {3:.4f}, 
      G_loss: {4:.4f}, L1_loss: {5:.4f}, TV_loss: {6:.4f}""".format(
                  loss_critic.item(), E_critic_gt.item(), E_critic_pred.item(), gp.item(),
                  loss_3d.item(), l1_err.item(), tv_err.item()))

            log.scalar_summary("lrg", scheduler_3d.optimizer.param_groups[0]['lr'], log_step)
            log.scalar_summary("lrd", optimizer_critic.param_groups[0]['lr'], log_step)
            
            log.scalar_summary("D_loss", loss_critic.item(), log_step)
            log.scalar_summary("E_critic_gt", E_critic_gt.item(), log_step)
            log.scalar_summary("E_critic_pred", E_critic_pred.item(), log_step)
            log.scalar_summary("gradient_penalty", gp.item(), log_step)
            log.scalar_summary("G_loss", loss_3d.item(), log_step)
            log.scalar_summary("L1_loss", l1_err.item(), log_step)
            log.scalar_summary("TV_loss", tv_err.item(), log_step)

        if i%300 == 0:
            samples_to_log=10
            indices = np.arange(batch_size*length)
            np.random.shuffle(indices)
            indices = sorted(indices[:samples_to_log])
            for ind in indices:
                sample_to_log = pred_2d[ind].data.cpu().numpy()
                log.image_summary('val_image_{}'.format(log_step), [sample_to_log], log_step)